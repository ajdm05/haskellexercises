module Exercises where
import Data.Char as Char
import System.IO

-- Exercise 3.1
resolve a b = div a b * 4

-- Exercise 3.2 
fstAndSndChar tuple = snd(fst(tuple))

-- Exercise 3.3
convertStringToBooleanList s = map Char.isLower s

-- Exercise 3.4
getLowerCaseLetters s = filter Char.isLower s
countLowerCaseLetters a = length(getLowerCaseLetters a)

-- Exercise 3.5
maxValueOfAList [] = 0
maxValueOfAList (a:b) = foldl max 0 (a:b)

-- Exercise 3.6
fstComponentOfSndElement ((a,b):(c,d):_) = c
											
-- Exercise 3.7
fibonacci 1 = 1
fibonacci 2 = 1
fibonacci num = fibonacci(num - 2) + fibonacci(num - 1)			

-- Exercise 3.8
multiplication a 0 = 0
multiplication 0 b = 0
multiplication a 1 = a
multiplication 1 b = b
multiplication a b = multiplication (a - 1) b + b								

-- Exercise 3.9
myMap fn [] = []
myMap fn (element:arr) = (fn element):(myMap fn arr)

-- Exercise 3.10
askNumbers = do
			    nums <- readNums
			    putStrLn ("Sum of Numbers: " ++ show(sum nums))
			    putStrLn ("Product of Numbers: " ++ show(producto nums))
			    printFactorials nums

readNums = do
			    putStrLn "Give me a number (or 0 to stop)"
			    num <- getLine
			    if (read num) == 0 then
			        return []
			    else do
			        allNums <- readNums
			        return ((read num ):allNums)

factorial 0 = 1
factorial 1 = 1
factorial n = factorial (n - 1) * n

printFactorials [] = return ()
printFactorials (a:bc) = do
						    putStrLn (show a ++ " factorial is " ++ show (factorial a))
						    printFactorials bc

producto a = product a

-- Exercise 4.4
data Triple a b c = Triple a b c
tripleFst (Triple a b c) = a	
tripleSnd (Triple a b c) = b
tripleThr (Triple a b c) = c

-- Exercise 4.5
data Quadruple a b c d= Quadruple a b c d
firstTwo (Quadruple a b c d) = a:b:[]
lastTwo (Quadruple a b c d) = c:d:[]

-- Exercise 4.6
data Tuple a b c d e = One a | Two a b
						| Three a b c | Four a b c d

tuple1(One a) = Just a
tuple1(Two a b) = Just a
tuple1(Three a b c) = Just a
tuple1(Four a b c d) = Just a

tuple2(One a) = Nothing
tuple2(Two a b) = Just b
tuple2(Three a b c) = Just b
tuple2(Four a b c d) = Just b

tuple3(One a) = Nothing
tuple3(Two a b) = Nothing
tuple3(Three a b c) = Just c
tuple3(Four a b c d) = Just c

tuple4(One a) = Nothing
tuple4(Two a b) = Nothing
tuple4(Three a b c) = Nothing
tuple4(Four a b c d) = Just d

-- Exercise 4.7
fromTuple(One a) = Left (Left a)
fromTuple(Two a b) = Left (Right (a,b))
fromTuple(Three a b c) = Right (Left (a,b,c))
fromTuple(Four a b c d) = Right (Right (a,b,c,d))

--Don't know how to run them 8,9,11
--Exercise 4.8
data List a = Nil | Cons a (List a)

listHead (Cons x xs) = x
listTail (Cons x xs) = xs

listFoldl f y Nil = y
listFoldl f y (Cons x xs) = listFoldl f (f y x) xs

listFoldr f y Nil = y
listFoldr f y (Cons x xs) = f x (listFoldr f x xs)

-- Exercise 4.9
data BinaryTree a = Leaf a | Branch (BinaryTree a) a (BinaryTree a)
elements (Leaf x) = [x]
elements (Branch lhs x rhs) = elements lhs ++ [x] ++ elements rhs

-- Exercise 4.10
foldTree f z (Leaf x) = f x z
foldTree f z (Branch lhs x rhs) = foldTree f (d x (foldTree f z rhs)) lhs
elements2 = foldTree (:) []

--Exercise 4.12